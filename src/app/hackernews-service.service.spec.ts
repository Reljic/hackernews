/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HackernewsServiceService } from './hackernews-service.service';

describe('HackernewsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HackernewsServiceService]
    });
  });

  it('should ...', inject([HackernewsServiceService], (service: HackernewsServiceService) => {
    expect(service).toBeTruthy();
  }));
});
