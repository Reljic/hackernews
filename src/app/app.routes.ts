import { Routes, RouterModule} from '@angular/router';
import { ItemCommentsComponent } from './item-comments/item-comments.component';
import { StoriesComponent } from './stories/stories.component';


const routes: Routes = [
    { path: '', component: StoriesComponent},
    { path: 'item/:id', component: ItemCommentsComponent}
];

export const routing = RouterModule.forRoot(routes);