import { Component, OnInit, Input } from '@angular/core';
import { HackernewsServiceService } from 'app/hackernews-service.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() itemID: number;
  item: any;

  constructor(private HackerNewsService: HackernewsServiceService) { }

  ngOnInit() {
    this.HackerNewsService.fetchItem(this.itemID).subscribe(data =>
      {this.item = data;
      }, error => console.log('Could not load item' + this.itemID));
  }

}
