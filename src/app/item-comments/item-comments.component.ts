import { Component, OnInit } from '@angular/core';
import { HackernewsServiceService } from 'app/hackernews-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-item-comments',
  templateUrl: './item-comments.component.html',
  styleUrls: ['./item-comments.component.css']
})
export class ItemCommentsComponent implements OnInit {

  sub: any;
  item: any;
  constructor(
    private hackerNewsService: HackernewsServiceService,
    private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let itemID =+ params['id'];
      this.hackerNewsService.fetchComments(itemID).subscribe(data => {
        this.item = data;
      }, error => console.log('could not load item' +itemID));
    })
  }

}
