import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HackernewsServiceService } from 'app/hackernews-service.service';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.css']
})
export class StoriesComponent implements OnInit {
    items;

  constructor(private hackerNewsService: HackernewsServiceService) { 
    
  }

  ngOnInit() {
    this.hackerNewsService.fetchStories().subscribe(items => this.items = items,
      error => console.log('Error fetching stories'));
  }

}
